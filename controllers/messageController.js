var User = require('../models/user');
var Config = require('../config');
const axios = require('axios');

exports.show_pinger = (req, res) => {
    res.render('ping', {title: "Send a Message"})
};

exports.do_ping = (req, res) => {
    const BASE_URL = "https://portal.bulksmsnigeria.net/api/?";
    axios.post(BASE_URL, {
        username: Config.bksms_email,
        password: Config.bksms_password,
        message: "Test Message from Pinger",
        sender: "Pinger Dev",
        recipients: "09074919539",
        response: 'json'
    })
        .then((response) => {
            res.json(response);
        })
        .catch((error) => {
            res.send(error);
        });
}