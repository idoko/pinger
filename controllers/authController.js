var User = require('../models/user');
var bcrypt = require('bcrypt');
const passport = require('passport');

const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

exports.show_login = (req, res, next) => {
    res.render('login', { title: "Login to Pinger" });
};

exports.do_login = passport.authenticate('local', { failureRedirect: '/login' }),
    (req, res) => {
        res.redirect('/ping');
};

exports.show_signup = (req, res, next) => {
    res.render('signup', { title: "Create a Pinger Account" });
};

exports.do_signup = [
    body('username', "Display name is required").isLength({ min: 1 }).trim(),
    sanitizeBody(body).trim().escape(),
    (req, res, next) => {
        const errors = validationResult(req);

        User.register(new User({ username: req.body.username }),
            req.body.password, (err, account) => {
                if (err) { return res.render('signup', { account: account, errors: err }); }
                passport.authenticate('local')(req, res, () => {
                    res.redirect('/ping');
                });
            })
    }
];
