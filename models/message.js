var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./user')

var MessageSchema = new Schema({
    sender: { type: Number, required: true },
    sender_name: { type: String, required: true },
    message: { type: String }
});

MessageSchema
    .virtual('user')
    .get(() => {
        User.findById(this.sender, (err, user) => {
            if (err) { return null; }
            return user;
        })
    });

module.exports = mongoose.model('Message', MessageSchema);