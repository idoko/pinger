var mongoose = require('mongoose');
var pasportLocalMongoose = require('passport-local-mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    username: { type: String, required: true, min: 2, max: 100 },
    email: { type: String },
    password: { type: String }
});

UserSchema.plugin(pasportLocalMongoose);

module.exports = mongoose.model('User', UserSchema);