var express = require('express');
var router = express.Router();
var auth_controller = require('../controllers/authController');
var message_controller = require('../controllers/messageController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Pinger' });
});

router.get('/login', auth_controller.show_login);
router.post('/login', auth_controller.do_login);

router.get('/signup', auth_controller.show_signup);
router.post('/signup', auth_controller.do_signup);

router.get('/ping', message_controller.show_pinger);
router.post('/ping', message_controller.do_ping);

module.exports = router;
